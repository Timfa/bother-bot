console.log("STARTING");
console.log("--------------------------------------------------");

const maxColor = 16777215;
const dayms = 86400000 - (3654321 * 2);
const minutems = 60000;

var Discord = require('discord.io');
var auth = require('./auth.json');
var fs = require('fs');
const { createHash } = require('crypto');

console.log("Requires loaded in.");

var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

console.log("initializing...");

var initialized = false;

bot.data = null;
bot.lastSave = Date.now();

bot.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function (err)
    {
        if (err)
        {
            console.log(err);
            bot.sendMessage({
                to: bot.data.lastServer,
                message: "Failed to create a back-up.",
                typing: false
            });
        }
        else
        {
            if (!silent)
            {
                bot.sendMessage({
                    to: bot.data.lastServer,
                    message: "Bot data back-up created.",
                    typing: false
                });
            }
        }
    });
};

bot.saveData = function (callback, context)
{
    console.log("Saving...");
    try
    {
        if (bot.data != null && bot.data != {} && initialized)
        {
            bot.data = bot.data || {};

            fs.writeFile("./botData.json", JSON.stringify(bot.data), function (err)
            {
                if (err)
                {
                    console.log(err);
                }

                context = context || this;
                callback && callback.call(context, true);
            });
        }
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

console.log("savedata function defined");

bot.on('ready', function (evt)
{
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');

    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch (e)
        {
            if (bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");

                    if (bot.data.lastServer)
                    {
                        bot.sendMessage({
                            to: bot.data.lastServer,
                            message: "A crash occurred, but I managed to restore a backup of my memory.",
                            typing: false
                        });
                        saveBackup = false;
                    }
                }
                catch (e)
                {
                    if (bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;

                        if (bot.data.lastServer)
                        {
                            bot.sendMessage({
                                to: bot.data.lastServer,
                                message: "A crash occurred, and my memory file was lost :(",
                                typing: false
                            });
                        }
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            bot.saveBackup(true);

            initialized = true;
        }

        bot.data = bot.data || {};

        bot.data.servers = bot.data.servers || {};

        console.log("Checking if I need to send update message...");

        if (bot.data.update && bot.data.update != "no")
        {
            bot.sendMessage({
                to: bot.data.update,
                message: "Updated!",
                typing: false
            });

            bot.data.update = "no";
        }
        else
        {
            console.log("no update notify to do");
        }
    });

    setTimeout(() => 
    {
        crashcrash();
    }, minutems * 15);

    setTimeout(() =>
    {
        Object.entries(bot.data.servers).forEach(entry => 
        {
            const [id, server] = entry;
            if (server.channel)
            {
                if (!server.time || server.time + dayms < Date.now())
                {
                    server.time = Date.now();

                    let mentions = Object.entries(server.mentions);
                    let chosen = mentions[Math.floor(Math.random() * mentions.length)];
                    let msg = bot.data.servers[id].message || "";

                    bot.sendMessage({
                        to: server.channel,
                        message: "<@!" + chosen[0] + ">",
                        typing: false
                    });

                    if (msg.length > 0)
                    {
                        setTimeout(function ()
                        {
                            bot.sendMessage({
                                to: server.channel,
                                message: msg,
                                typing: false
                            });
                        }, 1000);
                    }

                    if (bot.data.servers[id].previous)
                        bot.removeFromRole({ serverID: bot.data.servers[id].server, userID: bot.data.servers[id].previous, roleID: bot.data.servers[id].roleID });

                    if (bot.data.servers[id].roleID)
                        bot.addToRole({ serverID: bot.data.servers[id].server, userID: chosen[0], roleID: bot.data.servers[id].roleID });

                    bot.data.servers[id].previous = chosen[0];
                }
            }
        });

        bot.saveData();
    }, minutems);

});

console.log("onready defined");

bot.on('message', function (user, userID, channelID, message, evt)
{
    console.log(message);

    if (!initialized)
        return;

    var wasBot = evt.d.author.bot;

    var id = evt.d.guild_id || userID;

    if (!bot.data.servers[id])
    {
        bot.data.servers[id] = { mentions: {} };
    }

    var group = [userID];

    var words = message.split(' ');

    if (Date.now() > bot.lastSave + 3600000) // one hour
    {
        bot.saveBackup(true);
        bot.lastSave = Date.now();
    }

    bot.data.servers[id].server = id;

    if (!wasBot)
    {
        bot.data.servers[id].mentions[userID] = { time: Date.now() };

        var asArray = Object.entries(bot.data.servers[id].mentions);

        bot.data.servers[id].mentions = Object.fromEntries(asArray.filter(([key, value]) =>
        {
            return value.time + (dayms * 7) >= Date.now();
        }));
    }

    if (message.substring(0, 1) == '&') 
    {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
        args.splice(0, 1);

        var info = { user: user, userID: userID, channelID: channelID, message: message, evt: evt, serverId: id };

        if (cmd.toLowerCase() == "update")
        {
            crash();
        }

        if (cmd.toLowerCase() == "here")
        {
            bot.data.servers[id].channel = channelID;
            bot.sendMessage({
                to: bot.data.servers[id].channel,
                message: "OK",
                typing: false
            });
        }

        if (cmd.toLowerCase() == "message")
        {
            bot.data.servers[id].message = args.join(" ").trim();
            bot.sendMessage({
                to: bot.data.servers[id].channel,
                message: "OK",
                typing: false
            });
        }

        if (cmd.toLowerCase() == "role")
        {
            var validMention = args[1].match(/(<@&)([0-9]+)(>)/);
            console.log("attempot " + validMention + args);
            if (validMention)
            {
                mention = validMention[2];

                bot.sendMessage({
                    to: channelID,
                    message: "<@&" + mention + ">",
                    typing: false
                });

                console.log("ROLE " + mention);
                bot.data.servers[id].roleID = mention;
            }
        }

        if (cmd.toLowerCase() == "clear")
        {
            bot.data.servers[id].mentions = {};

            bot.sendMessage({
                to: channelID,
                message: "Cleared",
                typing: false
            });
        }

        if (cmd.toLowerCase() == "show")
        {
            bot.sendMessage({
                to: channelID,
                message: "```\n" + JSON.stringify(bot.data.servers[id]) + "\n```",
                typing: false
            });
        }
    }

    bot.saveData();
});

console.log("onmessage defined");
